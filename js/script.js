"use strict";
window.addEventListener('load', loading(), false);

function loading() {
	var main = document.getElementById("main");
	var loader = document.getElementById("loader");
	main.style.display = "block";
	loader.style.display = "none";
}

$('.navbar-nav>li>a').on('click', function(){
    $('.navbar-collapse').collapse('hide');
});

window.onscroll = function () {
	var scrollTopSize = window.pageYOffset || document.documentElement.scrollTop;
	var nav = document.getElementById('nav');
	var brandTitle = document.getElementById('brandTitle')
	var brandLogo = document.getElementById('brandLogo');
	//    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 60;
	//    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	if (scrollTopSize > 0) {
		nav.style.backgroundColor = "#000";
		brandTitle.style.fontSize = "1.2em";
		brandLogo.style.height = "1.4em";
	}
	if (scrollTopSize <= 0) {
		nav.style.backgroundColor = "transparent";
		brandTitle.style.fontSize = "1.8em";
		brandLogo.style.height = "1.8em";
	}
}

window.sr = ScrollReveal();
sr.reveal('.homeInnerText', {
	duration: 1000,
	origin: 'left',
});
sr.reveal('.exploreText', {
	duration: 1000,
	origin: 'left',
});
sr.reveal('.connectLeft', {
	duration: 1000,
	origin: 'left',
});
sr.reveal('.connectRight', {
	duration: 1000,
	origin: 'right',
});
sr.reveal('.create', {
	duration: 1000,
	scale: 0.7,
});
sr.reveal('.passionLeft', {
	duration: 1000,
	origin: 'left',
});
sr.reveal('.passionRight', {
	duration: 1000,
	origin: 'right',
});
sr.reveal('.share', {
	duration: 1000,
	scale: 0.6,
})
sr.reveal('.shareLeft', {
	duration: 1000,
	origin: 'left',
});
sr.reveal('.shareRight', {
	duration: 1000,
	origin: 'right',
});
sr.reveal('.footerText', {
	duration: 1000,
	origin: 'bottom',
});
